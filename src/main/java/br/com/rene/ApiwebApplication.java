package br.com.rene;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiwebApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApiwebApplication.class, args);
	}

}
