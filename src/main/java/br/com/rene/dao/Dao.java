package br.com.rene.dao;



import br.com.rene.bean.Bean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
public interface Dao<T extends Bean , ID extends Serializable> extends JpaRepository<T, ID> {


}
