package br.com.rene.dao;

import br.com.rene.bean.Cidade;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DaoCidade extends Dao<Cidade, Integer> {

	@Query("Select c from Cidade as c where c.capital <> '' order by c.name")
	List<Cidade> somenteCidadesCapitaisOrdenadaspNome();

	@Query("select count(c.name) as qt , c.uf from Cidade as c group by c.uf order by qt desc")
	List findQTCidadePorEstado();

	List<Cidade> findByibgeid(String ibge);
}
