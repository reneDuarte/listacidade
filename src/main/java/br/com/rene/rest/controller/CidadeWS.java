package br.com.rene.rest.controller;

import br.com.rene.bean.Cidade;
import br.com.rene.service.ServiceCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author rene.duarte
 * <p>
 * metodos utiizados
 * '@POST'
 * '@DELETE'
 * '@PUT'
 * '@GET'
 * <p>
 * tomei essa desição para demostrar sus funcionalidades
 */

@RestController
@RequestMapping("/rest/comando/cidade")
public class CidadeWS {

    /**
     * injeção do spring
     */
    @Autowired
    private ServiceCliente service;


	@RequestMapping(method =  { RequestMethod.GET, RequestMethod.POST }, value = "/carregardalista", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Retorno> carregarDalista() {
		Retorno retorno = new Retorno();
		try {
			service.carregarCidadeDoAquivo();
			retorno.setSucesso(true);
		} catch (Exception e) {
			/**
			 * configura o objeto de retorno do rest com a mensagem de erro
			 */
			retorno.setErro(e.getMessage());
		}

		return new ResponseEntity<>(retorno, HttpStatus.CREATED);
	}

	/**
	 *  Retornar somente as cidades que são capitais ordenadas por nome;
	 * @return
	 */

	@RequestMapping(method =  { RequestMethod.GET, RequestMethod.POST }, value = "/somenteCidadesCapitaisOrdenadaspNome", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Retorno> somenteCidadesCapitaisOrdenadaspNome() {
		Retorno retorno = new Retorno();
		try {
			List lista = service.somenteCidadesCapitaisOrdenadaspNome();
			retorno.setCustomerList(lista);
			retorno.setSucesso(true);
		} catch (Exception e) {
			/**
			 * configura o objeto de retorno do rest com a mensagem de erro
			 */
			retorno.setErro(e.getMessage());
		}

		return new ResponseEntity<>(retorno, HttpStatus.CREATED);
	}

	/**
	 * 4. Retornar a quantidade de cidades por estado;
	 * @return
	 */
	@RequestMapping(method =  { RequestMethod.GET, RequestMethod.POST }, value = "/quantidaCidadesPoestado", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Retorno> quantidaCidadesPoestado() {
		Retorno retorno = new Retorno();
		try {
			List lista = service.quantidaCidadesPorEstado();
			retorno.setCustomerList(lista);
			retorno.setSucesso(true);
		} catch (Exception e) {
			/**
			 * configura o objeto de retorno do rest com a mensagem de erro
			 */
			retorno.setErro(e.getMessage());
		}

		return new ResponseEntity<>(retorno, HttpStatus.CREATED);
	}

	/**
	 * Retornar o nome do estado com a maior e menor quantidade de cidades e a  quantidade de cidades;
	 * @return
	 */

	@RequestMapping(method =  { RequestMethod.GET, RequestMethod.POST }, value = "/retornarEstadoMaiorMenorquantidadeCidades", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Retorno> retornarEstadoMaiorMenorquantidadeCidades() {
		Retorno retorno = new Retorno();
		try {
			List lista = service.retornarEstadoMaiorMenorquantidadeCidades();
			retorno.setCustomerList(lista);
			retorno.setSucesso(true);
		} catch (Exception e) {
			/**
			 * configura o objeto de retorno do rest com a mensagem de erro
			 */
			retorno.setErro(e.getMessage());
		}

		return new ResponseEntity<>(retorno, HttpStatus.CREATED);
	}

	/**
	 * Obter os dados da cidade informando o id do IBGE;
	 * @param ibge_id
	 * @return
	 */
	@RequestMapping(method =  { RequestMethod.GET, RequestMethod.POST }, value = "/findIBGE/{ibge_id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Retorno> findIBGE(@PathVariable String ibge_id) {
		Retorno retorno = new Retorno();
		try {
			List<Cidade> lista = service.findByIbge_id(ibge_id);
			retorno.setCustomerList(lista);
			retorno.setSucesso(true);
		} catch (Exception e) {
			/**
			 * configura o objeto de retorno do rest com a mensagem de erro
			 */
			retorno.setErro(e.getMessage());
		}

		return new ResponseEntity<>(retorno, HttpStatus.CREATED);
	}

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{ID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Retorno> deletar(@PathVariable String ID) {

        Retorno retorno = new Retorno();
        try {
            /**
             * verifica parametro inválido
             */

            Cidade cliente = new Cidade();


            /**
             * configura o objeto de retorno do rest
             */
            retorno.setSucesso(true);


        } catch (Exception e) {
            /**
             * configura o objeto de retorno do rest com a mensagem de erro
             */
            retorno.setErro(e.getMessage());
        }

        return new ResponseEntity<>(retorno, HttpStatus.CREATED);
    }


}
