package br.com.rene.service;


import br.com.rene.dao.DaoCidade;
import br.com.rene.util.UtilCSV;
import br.com.rene.bean.Cidade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * serviço do Spring
 */
@Service
public class ServiceCliente {

    @Autowired
    private DaoCidade dao;


    public Cidade salvar(Cidade bean) {
        dao.save(bean);
        return bean;
    }


    public void deletar(Cidade bean) {
        dao.delete(bean);
    }

    public List<Cidade> listarCidade() {
        return (List<Cidade>) dao.findAll();
    }

    public void carregarCidadeDoAquivo() throws Exception {
	    String file =  System.class.getResource("/cidades.csv").getFile();
	    List<Cidade>  listToBean =  UtilCSV.listToBean(file, Cidade.class);
	    for (Cidade cidade : listToBean) {
		    salvar(cidade);
	    }
    }


	public List<Cidade> somenteCidadesCapitaisOrdenadaspNome() {
    	return (List<Cidade>) dao.somenteCidadesCapitaisOrdenadaspNome();
	}

	public List retornarEstadoMaiorMenorquantidadeCidades() {
    	List<Cidade> cidades =  dao.findQTCidadePorEstado();

		List<Cidade> cidadesRet = new ArrayList<>();
		if (cidades.size() > 0) {
			cidadesRet.add(cidades.get(0));
		}
		if (cidades.size() > 1) {
			cidadesRet.add(cidades.get(cidades.size() - 1));
		}
    	return cidadesRet;
	}

	public List quantidaCidadesPorEstado() {
		return dao.findQTCidadePorEstado();
	}

	public List<Cidade> findByIbge_id(String Ibge_id) {
    	return (List<Cidade>) dao.findByibgeid(Ibge_id);
	}
}
