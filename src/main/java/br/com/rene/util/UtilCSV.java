package br.com.rene.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;


public class UtilCSV {

    public static Integer findColuna(String colunaFind, String cvsSplitBy, String csvFile) throws IOException {
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            if ((line = br.readLine()) != null) {
                String[] colunas = line.split(cvsSplitBy);
                for (int i = 0; i <= colunas.length; i++) {
                    if (colunaFind.equals(colunas[i])) {
                        return i;
                    }
                }
            }
        }
        return null;
    }

    public static int countDistinctyColuna(int numeroColuna, String cvsSplitBy, BufferedReader br) throws IOException {
        String line;
        HashSet<String> valoresDistintos = new HashSet<>();
        while (numeroColuna > 0 && (line = br.readLine()) != null) {
            String valor = line.split(cvsSplitBy)[numeroColuna];
            if (valor != null && !"".equals(valor))
                valoresDistintos.add(valor);
        }
        return valoresDistintos.size();
    }

    public static int countDistinctyCSV(String nameColuna, String cvsSplitBy, String csvFile) {
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            Integer numeroColuna = UtilCSV.findColuna(nameColuna, cvsSplitBy, csvFile);
            br.readLine();
            if (numeroColuna != null)
                return UtilCSV.countDistinctyColuna(numeroColuna, cvsSplitBy, br);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static List<String> listTo(String csvFile, String cvsSplitBy, int qt, String nameColuna) {
        List<String> listaRet = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            Integer numeroColuna = UtilCSV.findColuna(nameColuna, cvsSplitBy, csvFile);
            br.readLine();
            if (numeroColuna != null) {
                String line;
                while (listaRet.size() < qt && (line = br.readLine()) != null) {
                    String valor = UtilCSV.getString(numeroColuna, cvsSplitBy, line);
                    listaRet.add(valor);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listaRet;
    }

    private static String getString(Integer numeroColuna, String cvsSplitBy, String line) {
        String[] colunas = line.split(cvsSplitBy);
        return colunas[numeroColuna];
    }


	public static  <T> List<T> listToBean(String csvFile,  Class<T> aClass) throws Exception {
		List<T>  tList =  new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
			String linha = br.readLine().replace("_" , "");

			String[] colunas = linha.split(",");

			while ((linha = br.readLine()) != null) {
				String[] valores = linha.split(",");
				T newBean = aClass.newInstance();

				for (int i = 0; i < colunas.length; i++) {
					Field field = newBean.getClass().getDeclaredField(colunas[i]);
					field.setAccessible(true);
					String valor = valores[i];
					field.set(newBean, valor);
				}
				tList.add(newBean);
			}

		}
		return tList;
	}
}
